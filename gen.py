#!/usr/bin/env python3
"""Parse ``lib.bib`` file and create the HTML from it."""
from pybtex import database as db

BIB_FILE = "lib.bib"
OUT_FILE = "index.html"
PREAMBLE = """<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>IID's F1/10 publications</title>
</head>
<body>

<h1>IID's F1/10 publications</h1>

<p>This is the list of <a href='https://iid.ciirc.cvut.cz/'>IID's</a>
publications related to <a href='https://f1tenth.org/'>F1/10</a>. You may <a
href='./lib.bib'>download the .bib file</a>.

<p>The source code of this page is on <a
href='https://gitlab.fel.cvut.cz/f1t/f1t.pages.fel.cvut.cz'>gitlab</a>.</p>

"""
FOOTER = "</body></html>"

if __name__ == "__main__":
    entries = db.parse_file(BIB_FILE).entries
    sentries = [v for (k, v) in entries.items()]
    sentries.sort(reverse=True, key=lambda x: x.fields["year"])
    with open(OUT_FILE, "w") as f:
        f.write(PREAMBLE)
        for v in sentries:
            f.write("<h2>{}</h2>".format(v.fields["title"]))
            f.write("<ul>")
            f.write("<li><a href='{t}'>{t}</a></li>".format(
                t=v.fields["url"],
            ))
            f.write("<li>{}</li>".format(v.fields["year"]))
            f.write("</ul>")
            f.write("<p>{}</p>".format(v.fields["abstract"]))
            f.write("<pre>{}</pre>".format(v.to_string("bibtex")))
        f.write(FOOTER)
